<?php

namespace App;

use Faker\Provider\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Blog extends Model
{
    protected $table = 'blogs';

    protected $fillable = [
        'name',
        'description',
        'meta_name',
        'meta_description',
        'slug',
        'status',
        'image'
        ];

    protected $appends = ['status_name'];

    public function GetStatusNameAttribute(){
        return $this->status == 1 ? 'Active' : 'Inactive';
    }
}
