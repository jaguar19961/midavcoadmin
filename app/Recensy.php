<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recensy extends Model
{
    protected $table = 'recensies';

    protected $fillable = [
        'image',
        'name',
        'description',
        'pozitions'
    ];
}
