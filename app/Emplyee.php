<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emplyee extends Model
{
    protected $table = 'employees';

    protected $fillable = ['name', 'image', 'number'];
}
