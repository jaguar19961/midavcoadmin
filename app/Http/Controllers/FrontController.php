<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Emplyee;
use App\Gallery;
use App\Recensy;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index(){
        $posts = Blog::orderBy('created_at', 'desc')->take(6)->get();
        $galleries = Gallery::get();
        $recenzies = Recensy::get();
        $empoyee = Emplyee::get();
        return view('site.pages.index', compact('posts', 'galleries', 'recenzies', 'empoyee'));
    }

    public function blogSingle($slug, $id){
        $post = Blog::findOrFail($id);

        $post_relevant = Blog::where('id', '<>', $id)->take(10)->get();
        return view('site.pages.blog_single', compact('post', 'post_relevant'));
    }
}
