<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index')->name('index');
Route::get('/blog/{slug}/{id}', 'FrontController@blogSingle')->name('blog.single');

Auth::routes(['register' => false]);

Route::group(['middleware' => 'auth','prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/dashboard', 'AdminController@index')->name('home');

    //--------- BLOG ---------//
    Route::get('/blog', 'BlogController@index')->name('blog');
    Route::get('/blog/show/{id}', 'BlogController@show')->name('blog.show');
    Route::post('/blog/store', 'BlogController@store')->name('blog.store');
    Route::get('/blog/create', 'BlogController@create')->name('blog.create');
    Route::post('/blog/update', 'BlogController@update')->name('blog.update');
    Route::post('/blog/edit', 'BlogController@edit')->name('blog.edit.page');
    Route::get('/blog/destroy/{id}', 'BlogController@destroy')->name('blog.destroy');
    //--------- End BLOG ---------//

    //--------- EMPLOYEE ---------//
    Route::get('/employee', 'EmployeeController@index')->name('employee');
    Route::get('/employee/show/{id}', 'EmployeeController@show')->name('employee.show');
    Route::post('/employee/store', 'EmployeeController@store')->name('employee.store');
    Route::get('/employee/create', 'EmployeeController@create')->name('employee.create');
    Route::post('/employee/update', 'EmployeeController@update')->name('employee.update');
    Route::post('/employee/edit', 'EmployeeController@edit')->name('employee.edit.page');
    Route::get('/employee/destroy/{id}', 'EmployeeController@destroy')->name('employee.destroy');
    //--------- End EMPLOYEE ---------//

    //--------- Testimonials -------//
    Route::get('/testimonial', 'TestimonialController@index')->name('testimonials');
    Route::get('/testimonials/show/{id}', 'TestimonialController@show')->name('testimonials.show');
    Route::post('/testimonials/store', 'TestimonialController@store')->name('testimonials.store');
    Route::get('/testimonials/create', 'TestimonialController@create')->name('testimonials.create');
    Route::post('/testimonials/update', 'TestimonialController@update')->name('testimonials.update');
    Route::post('/testimonials/edit', 'TestimonialController@edit')->name('testimonials.edit.page');
    Route::get('/testimonials/destroy/{id}', 'TestimonialController@destroy')->name('testimonials.destroy');
    //--------- END Testimonials -------//

    //--------- Gallery -------//
    Route::get('/gallery', 'GalleryController@index')->name('gallery');
    Route::get('/gallery/show/{id}', 'GalleryController@show')->name('gallery.show');
    Route::post('/gallery/store', 'GalleryController@store')->name('gallery.store');
    Route::get('/gallery/create', 'GalleryController@create')->name('gallery.create');
    Route::post('/gallery/update', 'GalleryController@update')->name('gallery.update');
    Route::post('/gallery/edit', 'GalleryController@edit')->name('gallery.edit.page');
    Route::get('/gallery/destroy/{id}', 'GalleryController@destroy')->name('gallery.destroy');
    //--------- END Gallery -------//
});

