@extends('site.layouts.app')
@section('content')
    <section id="blog_single">
        <div class="main_image">
            <img src="{{$post->image}}" alt="">
            <span class="title">{{$post->name}}</span>
        </div>
        <div class="container post">
            <div class="act-share">
                <div class="act">
                    <div class="share">
                        <a href="">Share</a>
                    </div>
                    <div class="date">
                        <span>{{$post->created_at->format('M d, Y')}}</span>
                    </div>
                </div>
                <div class="description">
                    {!! $post->description !!}
                </div>
            </div>
        </div>

        <div class="container another_posts">
            <slider-blog :posts="{{json_encode($post_relevant)}}"/>
        </div>
    </section>
@endsection
