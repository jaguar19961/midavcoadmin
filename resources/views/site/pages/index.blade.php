@extends('site.layouts.app')
@section('content')
    <section id="video">
        <div class="bg-video">
            <video autoplay muted loop playsinline poster="/img/video_img.jpg">
                <source src="/img/video/bg-video.mp4" type="video/mp4">
                <source src="/img/video_img.jpg" title="Your browser does not support the <video> tag" />
            </video>
        </div>
        <div class="content">
            <h1>Connecting top employers and employees</h1>
            <div class="ex">
                <span>Whether you search for employees or you want to find a well-paid job in Europe, we provide the best recruiting solution.</span>
            </div>
            <div class="act-btns">
                <button @click="dialogCompanies = true">Find qualified workers</button>
                <button @click="dialogWorkers = true">Find a well-paid job</button>
            </div>
        </div>
    </section>

    <section id="about-us">
        <div class="container">
            <div class="item">
                {{--                <span class="title">--}}
                {{--                About us--}}
                {{--                </span>--}}

                <div class="workers">
                    <span>
                        We'll find any worker or workplace you need
                    </span>
                    <div class="line"></div>
                </div>
                <div class="about">
                    <span>
                        The Recruitment Agency "AZ GLOBAL" was founded for clients who needed a recruitment service 12 years ago. We find workers for you in different countries: Romania, Ukraine, Moldova, Portugal, Poland and etc. During the activity, we have successfully covered over 18,600 vacancies and we have provided assistance in finding new employees for the largest international companies.
                    </span>
                </div>
                <div class="btn-about">
                    <button @click="dialogCompanies = true">I want to work with you</button>
                </div>
                <div class="timing">
                    <div class="tim">
                        <div class="nr">
                            <span>12</span>
                        </div>
                        <span>years of experience</span>
                    </div>
                    <div class="tim">
                        <div class="nr">
                            <span>24</span>
                        </div>
                        <span>months quality assurance</span>
                    </div>
                    <div class="tim">
                        <div class="nr">
                            <span>22</span>
                        </div>
                        <span>european countries</span>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="im">
                    <img src="/img/img/7.jpeg" alt="">
                </div>
                <div class="im">
                    <img src="/img/img/2.jpg" alt="">
                </div>
                <div class="im">
                    <img src="/img/img/3.jpg" alt="">
                </div>
                <div class="im">
                    <img src="/img/img/4.jpg" alt="">
                </div>
                <div class="im">
                    <img src="/img/img/5.jpg" alt="">
                </div>
                <div class="im">
                    <img src="/img/img/6.jpg" alt="">
                </div>
            </div>
        </div>
    </section>

    <section id="about-us-grid">
        <div class="container">
            <div class="title">
                <span>Jobs & Employees</span>
            </div>

            <div class="items">
                @foreach($empoyee as $item)
                    <div class="item @if(empty($item->image)) bue @endif">
                        <div class="img @if(!empty($item->image)) blur @endif">
                            @if(!empty($item->image))
                                <img src="{{asset($item->image)}}" alt="">
                            @endif
                        </div>
                        <div class="txt">
                            <span class="nr">{{$item->number}}</span><br>
                            <span class="text">{{$item->name}}</span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section id="how-it-works">
        <div class="container">
            <div class="title-pa">
                <span>HOW IT WORKS</span>
            </div>

            <div class="text_area">
                <div class="title">
                    <span>AZ Global is an imaginary bridge between the employer and employees all over the globe. We help connecting top employers with the best employees in any field.</span>
                </div>
                <div class="desc">
                    <ul>
                        <li>Unlimited market: we work with more than 22 countries;</li>
                        <li>Permanently updated database with open positions and qualified workers;</li>
                        <li>Detail-oriented work & interviews to find you the perfect worker or workplace;</li>
                        <li>Assistance on hiring process.</li>
                    </ul>
                </div>
                <div class="steps">
                    <ul class="progressbar">
                        <li>
                            <span class="tit">Lorem impus lorem postrr</span><br>
                            <span
                                class="exp">Lorem impus lorem postrr Lorem impus lorem postrr Lorem impus lorem postrr</span>
                        </li>
                        <li>
                            <span class="tit">Lorem impus lorem postrr</span><br>
                            <span
                                class="exp">Lorem impus lorem postrr Lorem impus lorem postrr Lorem impus lorem postrr</span>
                        </li>
                        <li>
                            <span class="tit">Lorem impus lorem postrr</span><br>
                            <span
                                class="exp">Lorem impus lorem postrr Lorem impus lorem postrr Lorem impus lorem postrr</span>
                        </li>
                    </ul>
                </div>

{{--                <div class="work-btn">--}}
{{--                    <button @click="formWorkers = true">Contact us or leave your contacts</button>--}}
{{--                </div>--}}

            </div>


        </div>
    </section>

    @if($galleries->count() > 0)
        <section id="gallery">
            <div class="container">
                <div class="title">
                    <span>Gallery</span>
                </div>
                <slider :galleries="{{json_encode($galleries)}}"/>
            </div>
        </section>
    @endif

    <section id="recenzii">
        <div class="container">
            <div class="title">
                <span>Testimonials</span>
            </div>
            <recenzii :recenzies="{{json_encode($recenzies)}}"></recenzii>
        </div>
    </section>

    @if($posts->count() > 0)
        <section id="blog">
            <div class="container">
                <div class="title">
                    <span>Blog</span>
                </div>
                <div class="items">
                    @foreach($posts as $key => $post)
                        <div class="item">
                            <div class="left">
                                <div class="img">
                                    <img src="{{$post->image}}" alt="">
                                </div>
                                <div class="date">
                                    <span class="day">{{$post->created_at->format('d')}}</span><br>
                                    <span class="month">{{$post->created_at->format('M')}}</span>
                                </div>
                            </div>
                            <div class="text">
                                <div class="desc">{{$post->name}}</div>
                                <div
                                    class="content">{{\Illuminate\Support\Str::limit($post->description, $limit = 100, $end = '...')}}</div>
                                <div class="read-more">
                                    <a href="{{route('blog.single',[$post->slug ,$post->id])}}">Read More ></a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    <section class="map">
        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d170347.25272038282!2d11.40175238859256!3d48.15505469475347!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479e75f9a38c5fd9%3A0x10cb84a7db1987d!2sM%C3%BCnchen%2C%20Germania!5e0!3m2!1sro!2s!4v1599122260946!5m2!1sro!2s"
            width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
            tabindex="0"></iframe>
    </section>
@endsection
