<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>{{env('APP_NAME')}}</title>
    <meta name="description" content="The HTML5 Herald">
    <meta name="author" content="midavco">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

</head>

<body>
<div id="app" :style="'height: 0px;'" v-cloak>
    @include('site.ui.header')
    @yield('content')
    @include('site.ui.footer')
    @include('site.ui.dialogs')
</div>
<script src="//code.jivosite.com/widget/RAnZ8QL0dM" async></script>
<script src="{{asset('js/app.js')}}" defer></script>
</body>
</html>
