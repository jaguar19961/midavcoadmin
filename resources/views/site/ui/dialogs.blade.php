<el-dialog
    title="Find qualified workers"
    :visible.sync="dialogCompanies"
    width="30%">
   <div>
       <el-input class="mb-2" placeholder="Company name" v-model="formCompany.company_name"></el-input>
       <country-select class="country_select mb-2" v-model="formCompany.country" :country="formCompany.country" topCountry="US"></country-select>
       <el-input placeholder="Company email" v-model="formCompany.email"></el-input>
   </div>
    <span slot="footer" class="dialog-footer">
    <el-button @click="dialogCompanies = false">Cancel</el-button>
    <el-button type="primary" @click="dialogCompanies = false">Confirm</el-button>
  </span>
</el-dialog>

<el-dialog
    title="Find a well-paid job"
    :visible.sync="dialogWorkers"
    width="30%">
   <div>
       <el-input class="mb-2" placeholder="First name" v-model="formWorkers.first_name"></el-input>
       <el-input class="mb-2" placeholder="Last name" v-model="formWorkers.last_name"></el-input>
       <div class="group mb-2">
           <span>EU Passport </span>
           <el-radio-group v-model="formWorkers.eu_passport">
               <el-radio-button label="Yes"></el-radio-button>
               <el-radio-button label="No"></el-radio-button>
           </el-radio-group>
       </div>
       <el-input placeholder="Contact phone" v-model="formWorkers.phone"></el-input>
   </div>
    <span slot="footer" class="dialog-footer">
    <el-button @click="dialogWorkers = false">Cancel</el-button>
    <el-button type="primary" @click="dialogWorkers = false">Confirm</el-button>
  </span>
</el-dialog>
