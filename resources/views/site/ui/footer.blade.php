<section id="footer">
    <div class="container">
        <div class="logo">
            <img src="/img/logo_all_white1.png" alt="">
        </div>
        <div class="work">
            <ul>
                <li class="p">Work schedule:</li>
                <li>Mon - Fri: 9:00 am–9:00 pm</li>
                <li>Sat: 10:00 am–5:00 pm</li>
            </ul>
            <div class="social">
                <img src="/img/icons/facebook.svg" alt="">
                <img src="/img/icons/instagram.svg" alt="">
                <img src="/img/icons/vk.svg" alt="">
            </div>
        </div>
        <div class="offices">
            <ul>
                <li class="p">Our offices:</li>
                <li>Germany</li>
                <li>Poland</li>
                <li>+4 (822) 602-27-48</li>
                <li>+4 (822) 602-27-49</li>
                <li>United Kingdom</li>
                <li></li>
                <li>+4 (478) 621-15-803</li>
                <li>+4 (420) 457-71-011</li>
                <li>office@azglobal.info</li>
            </ul>
        </div>
    </div>
</section>
