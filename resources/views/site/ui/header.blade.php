

<section id="header" :style="scrollPosition > 40 ? 'background: #154566' : ''">
    <div class="container">
        <div class="items">
            <div class="left">
                <div class="img" onclick="window.location.href = '/'">
{{--                    <img src="/img/logo-white.png" alt="">--}}
                    <img src="/img/logo_all_white1.png" alt="">
                </div>
            </div>
            <div class="rigth">
                <div class="menu">
                    <ul>
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li>
                            <a href="#how-it-works">HOW IT WORKS</a>
                        </li>
                        <li>
                            <a href="#recenzii">Testimonials</a>
                        </li>
                        <li>
                            <a href="#about-us">About us</a>
                        </li>
                        <li>
                            <a href="#blog">Blog</a>
                        </li>
                        <li>
                            <a href="#footer">Contact</a>
                        </li>
                    </ul>
                </div>
                <div class="social">
                    <div class="soc">
                        <img src="/img/icons/facebook.svg" alt="" onclick="window.location.href = 'https://www.facebook.com/AZGlobalinfo-105241681327849'">
                        <img src="/img/icons/instagram.svg" alt="">
                        <img src="/img/icons/vk.svg" alt="">
                        <img src="/img/icons/youtube-tv.svg" alt="">
                    </div>
                    <a class="mail_head" href="mailto:office@azglobal.info">office@azglobal.info</a>
                </div>
                <div @click="sidemenu = !sidemenu" class="burger">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    </div>
    <el-menu
        v-show="sidemenu"
        default-active="1"
        class="el-menu-vertical"
        @open="handleOpen"
        @close="handleClose">
        <el-menu-item index="1" onclick="window.location.href = '/'">
            <span>HOME</span>
        </el-menu-item>
        <el-menu-item index="2" onclick="window.location.href = '#how-it-works'">
            <span>HOW IT WORKS</span>
        </el-menu-item>
        <el-menu-item index="3" onclick="window.location.href = '#recenzii'">
            <span>TESTIMONIALS</span>
        </el-menu-item>
        <el-menu-item index="4" onclick="window.location.href = '#about-us'">
            <span>ABOUT US</span>
        </el-menu-item>
        <el-menu-item index="5" onclick="window.location.href = '#blog'">
            <span>BLOG</span>
        </el-menu-item>
        <el-menu-item index="6" onclick="window.location.href = '#footer'">
            <span>CONTACT</span>
        </el-menu-item>
    </el-menu>
</section>

