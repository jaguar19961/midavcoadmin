/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/en'

Vue.use(ElementUI, {locale})

import vueCountryRegionSelect from 'vue-country-region-select'
Vue.use(vueCountryRegionSelect)
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('slider', require('./components/Slider.vue').default);
Vue.component('slider-blog', require('./components/SliderBlog.vue').default);
Vue.component('recenzii', require('./components/Recenzii.vue').default);


import moment from 'moment';

Vue.prototype.$moment = moment;
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        scrollPosition: null,
        sidemenu: false,
        dialogCompanies: false,
        dialogWorkers: false,

        formCompany: {
            company_name: '',
            country: '',
            region: '',
            email: '',
        },

        formWorkers: {
            first_name: '',
            last_name: '',
            eu_passport: '',
            phone: '',
        }
    },

    methods: {
        updateScroll() {
            this.scrollPosition = window.scrollY
        },
        handleOpen(key, keyPath) {
            console.log(key, keyPath);
        },
        handleClose(key, keyPath) {
            console.log(key, keyPath);
        }
    },

    mounted() {
        window.addEventListener('scroll', this.updateScroll);
    },

    destroy() {
        window.removeEventListener('scroll', this.updateScroll)
    }
});
